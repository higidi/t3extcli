<?php

/*
 * This file is part of the Higidi/T3ExtCLI package.
 *
 * (c) Daniel Hürtgen <daniel@higidi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Higidi\T3ExtCli;

use Cilex\Application as BaseApplication;

/**
 * The T3ExtCLI application class.
 *
 * @author Daniel Hürtgen <daniel@higidi.com>
 *
 * @api
 */
class Application extends BaseApplication
{
}
